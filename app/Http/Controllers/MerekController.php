<?php

namespace App\Http\Controllers;

use App\Models\Merek;
use Illuminate\Http\Request;

class MerekController extends Controller
{
    public function index() {
        $merek = Merek::orderBy('id','desc')->paginate(5);
          
        return view('barang.databarang', compact('merek'));
       }
}
