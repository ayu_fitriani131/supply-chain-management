<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Merek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
class BarangController extends Controller
{
    public function index(Request $request){

        $barang = Barang::orderBy('id','desc')->paginate(5);
      

       
        return view('barang.databarang', compact('barang'));
    }

    public function tambahbarang(){
        return view('tambahbarang');
    }
    public function insertdata(Request $request){
        //dd($request->all());
        Barang::create($request->all());
        return redirect()->route('barang')->with('success','Data Berhasil Di Tambahkan');
    }
    public function tampilbarang($id){
        $data = Barang::find($id);
        //dd($data);

        return view('tampilbarang', compact('data'));
    }
    public function updatedata(Request $request, $id){
        $data = Barang::find($id);
        $data->update($request->all());
        if(session('halaman_url')){
            return Redirect(session('halaman_url'))->with('success','Data Berhasil Di Update');
        }

        return redirect()->route('barang')->with('success','Data Berhasil Di Update');
    }

    public function kategori(){
        $barangs = Barang::all();
    }
    public function delete($id){
        $data = Barang::find($id);
        $data->delete();
        return redirect()->route('barang')->with('success','Data Berhasil Di Hapus');
    }   
}
