<?php

namespace App\Http\Controllers;

use App\Models\Lokasi;
use Illuminate\Http\Request;

class LokasiController extends Controller
{
    public function index() {
        $lokasi = Lokasi::orderBy('id','desc')->paginate(5);
          
        return view('barang.databarang', compact('lokasi'));
       }
}
