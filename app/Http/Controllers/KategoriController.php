<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
   public function index() {
    $kategori = Kategori::orderBy('id','desc')->paginate(5);
      
    return view('barang.databarang', compact('kategori'));
   }
}
