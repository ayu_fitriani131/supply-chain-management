<?php


namespace App\Http\Controllers\API;

use Exception;
use App\Models\Barang;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

class BarangController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        // $limit = $request->input('limit', 6);
        // $kode_barang = $request->input('kode_barang');
        // $nama_barang = $request->input('nama_barang');
        // $stok = $request->input('stok');
        // $harga = $request->input('harga');
        // $merek_id = $request->input('merek_id');
        // $kategori_id = $request->input('kategori_id');
        // $lokasi_id = $request->input('lokasi_id');
        

        if($id)
        {
            $product = Barang::with('kode_barang')->find($id);
            if($product)
                return ResponseFormatter::success(
                    $product,
                    'Data produk berhasil diambil'
                );
            else
                return ResponseFormatter::error(
                    null,
                    'Data produk tidak ada',
                    404
                );
        }
        
    
    }

     /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    public function input(Request $request)
    {
        try {
            $request->validate([
                'kode_barang' => ['required', 'string', 'max:255'],
                'nama_barang' => ['required', 'string', 'max:255'],
                'stok' => ['required', 'string', 'max:255'],
                'harga' => ['required', 'string', 'max:255'],
                'merek_id' => ['required', 'string', 'max:255'],
                'kategori_id' => ['required', 'string', 'max:255'],
                'lokasi_id' => ['required', 'string', 'max:255'],
            ]);

            Barang::create([
                'kode_barang' => $request->kode_barang,
                'nama_barang' => $request->nama_barang,
                'stok' => $request->stok,
                'harga' => $request->harga,
                'merek_id' => $request->merek_id,
                'kategori_id' => $request->kategori_id,
                'lokasi_id' => $request->lokasi_id,
               
            ]);
           
            return ResponseFormatter::success([
            ],'Barang telah berhasil dimasukan');
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error,
            ],'Gagal menambahkan barang!', 500);
        }
    }

}