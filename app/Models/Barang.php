<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    
    protected $primarykey = 'barang_id';
    protected $fillable = [
    'kode_barang',
    'nama_barang',
    'stok',
    'harga',
    'merek_id',
    'kategori_id',
    'lokasi_id'];

    public function merek()
    {
        return $this->belongsTo(Merek::class, 'merek_id', 'id');
    }
    
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id', 'id');
    }

    public function lokasi()
    {
        return $this->belongsTo(Lokasi::class, 'lokasi_id', 'id');
    }

   
}
