<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{

    use HasFactory;
    protected $primaykey = 'merek_id';
    protected $fillable =[
        'barang_id', 
    ];
    
    public function barang()
    {
        return $this->hasMany(Barang::class, 'merek_id', 'id');
    }
}
