<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
            Schema::create('barangs', function (Blueprint $table) {
                $table->id();
                $table->bigInteger('kode_barang');
                $table->char('nama_barang');
                $table->Integer('stok');
                $table->Integer('harga');
                $table->bigInteger('merek_id');
                $table->bigInteger('kategori_id');
                $table->bigInteger('lokasi_id');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
