<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barangs')->insert([
            'kode_barang' => '1',
            'nama_barang' => 'AC SAMSUNG 2 PK',
            'stok' => '15',
            'harga' => '3000000',
            'merek_id' => '1',
            'kategori_id' =>'1',
            'lokasi_id' => '1',
            
        ]);
    }
}
